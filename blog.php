<?php include 'bar.php';?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Education Template - Blog</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<!--
Template 2067 Education
http://www.tooplate.com/view/2067-education
-->
<link href="tooplate_style.css" rel="stylesheet" type="text/css" />

<script type="text/JavaScript" src="js/jquery-1.6.3.js"></script> 

<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />

<script type="text/javascript" src="js/ddsmoothmenu.js">

/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/

</script>

<script type="text/javascript">

ddsmoothmenu.init({
	mainmenuid: "tooplate_menu", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script>

<link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" /> 
<script type="text/JavaScript" src="js/slimbox2.js"></script> 

<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />

</head>
<body>

<div id="tooplate_main">
<div id="tooplate_content" class="left">
<?php include 'connecter.php';

    $sql = "SELECT * FROM `blog`";
    $query = mysqli_query($conn,$sql);

	$num_rows = mysqli_num_rows($query);

	$per_page = 2;   // Per Page
	$page  = 1;
	
	if(isset($_GET["Page"]))
	{
		$page = $_GET["Page"];
	}

	$prev_page = $page-1;
	$next_page = $page+1;

	$row_start = (($per_page*$page)-$per_page);
	if($num_rows<=$per_page)
	{
		$num_pages =1;
	}
	else if(($num_rows % $per_page)==0)
	{
		$num_pages =($num_rows/$per_page) ;
	}
	else
	{
		$num_pages =($num_rows/$per_page)+1;
		$num_pages = (int)$num_pages;
	}
	$row_end = $per_page * $page;
	if($row_end > $num_rows)
	{
		$row_end = $num_rows;
	}

    $sql .= " ORDER BY blogid ASC LIMIT $row_start ,$row_end ";

    $query  = mysqli_query($conn,$sql)or die( mysqli_error($conn) . "<br>$sql");
    ?>
    

    <?php

    while($result=mysqli_fetch_array($query,MYSQLI_ASSOC))
    {
    ?>
        	<div class="post">
            <h2><a href="blogfull.php?blogid=<?php echo $result["blogid"];?>"><?php echo $result["name"];?></a></h2>
            <img class="img_border img_border_b img_nom"  src ="form/image/<?php echo $result['data']?>" alt="Post Image"  width="400px"hight = "300"/>
                <div class="post_meta">
                    <span class="post_author"><a href="#">Admin</a></span>
                    <span class="date"><a href="#"><?php echo $result["date"];?></a></span>
                    <span class="tag"><a href="#">Media</a>, <a href="#">Creation</a></span>
                    <span class="comment"><a href="#">15 comments</a></span>
				</div> 
                <p> <div>
                <?php 
                   $detail = $result["detail"];
                   echo substr($detail, 1, 400);
                ?>
                </div>
                      </p>
                <a class="more" href="blogfull.php?blogid=<?php echo $result["blogid"];?>">More</a>
			</div>
            <?php
                    
}
?>

        	<div class="pagging">
                <ul>
                <br>
                Total <?php echo $num_rows;?> Record : <?php echo $num_pages;?> Page :
                <?php
                if($prev_page)
                {
                    echo " <a href='$_SERVER[SCRIPT_NAME]?Page=$prev_page'><< Back</a> ";
                }
                
                for($i=1; $i<=$num_pages; $i++){
                    if($i != $page)
                    {
                        echo "[ <a href='$_SERVER[SCRIPT_NAME]?Page=$i'>$i</a> ]";
                    }
                    else
                    {
                        echo "<b> $i </b>";
                    }
                }
                if($page!=$num_pages)
                {
                    echo " <a href ='$_SERVER[SCRIPT_NAME]?Page=$next_page'>Next>></a> ";
                }
                $conn = null;      
                ?>    </ul>
                <div class="clear"></div>
            </div>
        </div>
        <div id="tooplate_sidebar" class="right">
        	<div class="content_wrapper content_mb_60">
                <h3>Categories</h3>
                <ul class="sidebar_link">
                     <li><a href="#">Consectetur adipiscing eli</a></li>
                    <li><a href="#">Nullam vulputate est</a></li>
                    <li><a href="#">Duis porta velit</a></li>
                    <li><a href="#">Pretium suscipit</a></li>
                    <li><a href="#">Nullam eu diam</a></li>
                    <li><a href="#">Duis in libero est</a></li>
                    <li><a href="#">Aenean tincidunt</a></li>
                    <li><a href="#">Morbi tempus iaculis</a></li>
                </ul>
			</div>
            
          <div class="content_wrapper content_mb_60">
                <h3>Recent Comments</h3>
                <ul class="comment_list">
                    <li>
                    	<span>Donec rhoncus, neque quis dapibus dapibus, lorem tortor semper est...</span>
                        <span class="comment_meta">
		                    <strong>David</strong> on <a href="#">Curabitur Mollis Justo</a>
						</span>
					</li>
                    <li>
                    	<span>Aliquam erat volutpat. Vestibulum urna libero, fringilla eu faucibus...</span>
                        <span class="comment_meta">
		                    <strong>Jame</strong> on <a href="#">Curabitur Mollis Justo</a>
						</span>
					</li>
                    <li>
                    	<span>Nec fringilla eget elit. Nullam tincidunt, felis rhoncus rhoncus...</span>
                        <span class="comment_meta">
		                    <strong>Simon</strong> on <a href="#">Curabitur Mollis Justo</a>
						</span>
					</li>
                    <li>
                    	<span>Ultricies turpis lectus tristique nulla, nec feugiat felis sapien...</span>
                        <span class="comment_meta">
		                    <strong>Snow</strong> on <a href="#">Curabitur Mollis Justo</a>
						</span>
					</li>
                </ul>
            </div>
            
            <div class="content_wrapper content_mb_60">
                <h3>Blogroll</h3>
                <ul class="sidebar_link">
                    <li><a href="#">Nullam eu diam</a></li>
                    <li><a href="#">Duis in libero est</a></li>
                    <li><a href="#">Aenean tincidunt</a></li>
                    <li><a href="#">Morbi tempus iaculis</a></li>
                    <li><a href="#">Consectetur adipiscing eli</a></li>
                    <li><a href="#">Nullam vulputate est</a></li>
                    <li><a href="#">Duis porta velit</a></li>
                    <li><a href="#">Pretium suscipit</a></li>
                </ul>
			</div>
        </div>  		
        
        <div class="clear h30"></div>
        <div style="display:none;" class="nav_up" id="nav_up"></div>
    </div>    <!-- END of tooplate_main -->


<script src="js/scroll-startstop.events.jquery.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function() {
		var $elem = $('#content');
		
		$('#nav_up').fadeIn('slow');
		
		$(window).bind('scrollstart', function(){
			$('#nav_up,#nav_down').stop().animate({'opacity':'0.2'});
		});
		$(window).bind('scrollstop', function(){
			$('#nav_up,#nav_down').stop().animate({'opacity':'1'});
		});
		
		$('#nav_up').click(
			function (e) {
				$('html, body').animate({scrollTop: '0px'}, 800);
			}
		);
	});
</script>

</body>
</html>
<?php include 'footer.php';?>
